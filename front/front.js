const express = require("express");
var axios = require("axios");
var bodyparser = require("body-parser");

var app = express();
app.set("view-engine", "ejs");

app.use(bodyparser.json());
app.use(bodyparser.urlencoded({ extended: false }));

app.post("/form", function(req, res) {
    console.log(req.body);
    let user = req.body;
    axios.post("http://localhost:3000/user/new", user);
    res.render("uservalide.ejs"); //affiche la page statique "inscription bien enregistrée"
});

app.get("/form", function(req, res) {
    res.render("form.ejs");
});

app.get("/userlist", function(req, res) {
    axios.get("http://localhost:3000/user/")
    .then(function(axiosRes){
        var userTable = axiosRes.data;
        res.render("userlist.ejs", {userTable});
    })
    .catch(function(error){
        console.log(error);
    })
});

app.post("/userlist", function(req, res) {
    let name = req.body.deleteName;
    axios.delete("http://localhost:3000/user/" + name)
    .then(function(response) {
        axios.get("http://localhost:3000/user")
            .then(function (reponse) {
                let tableau = reponse.data;
                res.render("userlist.ejs", { 'userTable': tableau });
            })
            .catch(function (error) {
                console.log(error);
            });
    })
    .catch (function(error) {
    });
});

app.listen(8080);

console.log("serveur en écoute au port 8080");