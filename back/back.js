const express = require("express");
const bodyparser = require("body-parser");
const fs = require("fs");

const app = express();
var tableau = [];

app.use(bodyparser.json());
app.use(bodyparser.urlencoded({extended: false}));

if (!fs.existsSync("infos.json")) {
    fs.writeFileSync("infos.json", JSON.stringify(tableau));
}   else {
    tableau = fs.readFileSync("infos.json");
    try {
        tableau = JSON.parse(tableau);
    }
    catch (error){
        tableau = [];
    }
}

app.post("/user/new", function(req,res) {
    let user = req.body;
    tableau.push(user);
    fs.writeFileSync("infos.json", JSON.stringify(tableau));
    res.send();
    //Enregistre le nouvel utilisateur
});

app.get("/user", function(req,res) {
    res.send(tableau);
    //Renvoie le tableau d'utilisateurs
});

app.delete("/user/:name", function(req,res) {
//    //Supprimer un utilisateur par son nom
   let deleteName = req.params.name;
   tableau = tableau.filter(user => user.name !== deleteName);
    fs.writeFileSync("infos.json", JSON.stringify(tableau));
    res.send();
   });


app.listen(3000);

console.log("serveur en écoute au port 3000");